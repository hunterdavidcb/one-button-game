﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSickos : MonoBehaviour
{
	public GameObject sickoPrefab;
	public float delay = 10f;
	public TextUpdate tu;
    // Start is called before the first frame update
    void Start()
    {
		StartCoroutine(Spawn());
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	IEnumerator Spawn()
	{
		while (true)
		{
			GameObject sicko = Instantiate(sickoPrefab);
			Vector2 pos = (Random.insideUnitCircle * 8f) + new Vector2(transform.position.x, transform.position.y);
			sicko.transform.position = new Vector3(pos.x, pos.y, 0f);

			sicko.GetComponent<Sicko>().Healed += tu.OnHealed;
			yield return new WaitForSeconds(delay);
		}
	}
}
