﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
	public delegate void PickUpHandler(int n);
	public event PickUpHandler PickedUp;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
		//Debug.Log("entered");
		//Debug.Log(collision.transform.tag);
		if (collision.transform.tag.Equals("HealthBox"))
		{
			//Debug.Log("entered");
			if (PickedUp != null)
			{
				PickedUp(5);
			}
			Destroy(collision.gameObject);
		}
	}
}
