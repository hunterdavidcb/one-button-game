﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetSick : MonoBehaviour
{
	public SpriteRenderer[] sprites;
	Color orig = new Color(1, 1, 1, 1);
	Color red = new Color(1, 0, 0, 1);
	float sick = 0f;

	public delegate void DeathHandler();
	public event DeathHandler Died;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
		//Debug.Log("entered");
		//Debug.Log(collision.transform.tag);
		if (collision.transform.tag.Equals("Sick"))
		{
			Debug.Log("entered");
			StartCoroutine(AdjustSickness(true));
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.transform.tag == "Sick")
		{
			Debug.Log("exited");
			StopAllCoroutines();
			StartCoroutine(AdjustSickness(false));
		}
	}

	IEnumerator AdjustSickness(bool pos)
	{
		while (true)
		{
			sick = pos? sick + Time.deltaTime * 5f : sick - Time.deltaTime * 5f;
			if (sick >= 100)
			{
				Die();
			}

			if (sick <= 0)
			{
				StopAllCoroutines();
			}

			Debug.Log("still running");
			//Debug.Log(sick);
			foreach (var item in sprites)
			{
				item.color = Color.Lerp(orig, red, sick / 100);
				//Debug.Log("here");
			}
			yield return null;
		}
		
	}

	private void Die()
	{
		Debug.Log("dead");
		if (Died != null)
		{
			Died();
		}

		StopAllCoroutines();
	}

	//if the rigid bodies don't move, this basically goes to sleep, so it is not reliable


	//private void OnTriggerStay2D(Collider2D collision)
	//{
	//	if (collision.transform.tag == "Sick")
	//	{
	//		sick += Time.deltaTime * 5f;
	//		Debug.Log(sick);
	//		foreach (var item in sprites)
	//		{
	//			item.color = Color.Lerp(orig, red, sick/100);
	//			//Debug.Log("here");
	//		}
	//		//Debug.Log("staying");
	//	}
	//}
}
