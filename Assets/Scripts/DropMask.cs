﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropMask : MonoBehaviour
{
	public Transform maskPrefab;
	bool possible = true;
	float delay = 0.5f;
	float last;

	public delegate void DropMaskHandler(GameObject go);
	public event DropMaskHandler Dropped;
	// Start is called before the first frame update
	void Start()
    {
		GetComponent<Inventory>().InventoryChanged += OnInventoryChanged;
		GetComponent<GetSick>().Died += OnDied;
    }

	private void OnDied()
	{
		this.enabled = false;
	}

	private void OnInventoryChanged(int c)
	{
		possible = c > 0;
		Debug.Log(possible);
	}

	// Update is called once per frame
	void Update()
    {
		if (Input.GetMouseButtonDown(0) && Time.time - last >= delay && possible)
		{
			//spawn a mask
			GameObject go = Instantiate(maskPrefab.gameObject);
			//set the position, etc
			go.transform.position = transform.position;

			last = Time.time;
			if (Dropped != null)
			{
				Dropped(go);
			}
		}
    }


}
