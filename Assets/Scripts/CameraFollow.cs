﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	public Transform target;
	Vector3 v;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		transform.position = Vector3.SmoothDamp(transform.position, 
			new Vector3(target.position.x,target.position.y,transform.position.z), ref v, 1f);
    }
}
