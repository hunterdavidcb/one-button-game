﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sicko : MonoBehaviour
{
	Animator anim;
	Transform player;
	Vector2 v;
	bool isRight = false;
	public SpriteRenderer[] sprites;
	Color orig = new Color(1, 0, 0, 1);
	Color white = new Color(1, 1, 1, 1);
	float sick = 100f;

	bool healing = false;

	public delegate void HealHandler();
	public event HealHandler Healed;
	// Start is called before the first frame update
	void Awake()
    {
		anim = GetComponent<Animator>();
		player = GameObject.FindGameObjectWithTag("Player").transform;

		player.GetComponent<DropMask>().Dropped += OnMaskDropped;
    }

	private void OnMaskDropped(GameObject go)
	{
		//we will seek the mask instead
		if (!healing)
		{
			//we get the first child, because this is the actual mask
			player = go.transform.GetChild(0);
		}
		
	}

	// Update is called once per frame
	void Update()
    {
		if (player != null)
		{
			if (Vector2.Distance(transform.position, player.position) > 0.5f)
			{
				float s = Mathf.Clamp(Vector2.Distance(transform.position, player.position) / 2f, 2f,8f);
				transform.position = Vector2.SmoothDamp(transform.position, player.position, ref v, 40 / s);
				anim.SetFloat("speed", s);
			}
			else
			{
				anim.SetFloat("speed", 0);
			}

			//use the dot product to check if the mouse is on the left or the right
			if (Vector3.Dot(Vector3.left, player.position - transform.position) < 0f && !isRight)
			{
				//right facing
				isRight = true;
				transform.rotation = Quaternion.Euler(0, 180, 0);
			}
			else if (Vector3.Dot(Vector3.left, player.position - transform.position) > 0f && isRight)
			{
				//left facing
				isRight = false;
				transform.rotation = Quaternion.Euler(0, 0, 0);
			}
		}
		else
		{
			if (!healing)
			{
				Debug.Log("here" + gameObject.name);
				player = GameObject.FindGameObjectWithTag("Player").transform;
			}
		}
		
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.transform.gameObject.tag == "Mask")
		{
			//Debug.Log("should stop");
			Destroy(collision.transform.root.gameObject);
			healing = true;
			anim.SetFloat("speed", 0);
			StartCoroutine(AdjustSickness());
			//pick up the mask, stop chasing the player
		}
	}

	IEnumerator AdjustSickness()
	{
		while (true)
		{
			sick = sick - Time.deltaTime * 5f;

			//Debug.Log(sick);
			if (sick <= 0)
			{
				ChangeTag(transform);
				if (Healed != null )
				{
					Healed();
				}
				StopAllCoroutines();
			}

			//Debug.Log("still running");
			//Debug.Log(sick);
			foreach (var item in sprites)
			{
				item.color = Color.Lerp(orig, white, (100f-sick) / 100);
				//Debug.Log("here");
			}
			yield return null;
		}

	}

	void ChangeTag(Transform t)
	{
		t.tag = "Healthy";
		//recurse through the children, the children's children, etc.
		for (int i = 0; i < t.childCount; i++)
		{
			ChangeTag(t.GetChild(i));
		}
	}
}
