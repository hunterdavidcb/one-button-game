﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextUpdate : MonoBehaviour
{
	Text text;
	float score = 0f;
	AudioSource source;
    // Start is called before the first frame update
    void Start()
    {
		text = GetComponent<Text>();
		source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
		score += Time.deltaTime;
		text.text = score.ToString("00.0");
    }

	public void OnHealed()
	{
		source.Play();
		score += 100f;
	}
}
