﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBoxes : MonoBehaviour
{
	public GameObject boxPrefab;

	public float delay = 30f;
	// Start is called before the first frame update
	void Start()
    {
		StartCoroutine(Spawn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	IEnumerator Spawn()
	{
		while (true)
		{
			GameObject box = Instantiate(boxPrefab);
			Vector2 pos = (Random.insideUnitCircle * 8f) + new Vector2(transform.position.x, transform.position.y);
			box.transform.position = new Vector3(pos.x, pos.y, 0f);


			yield return new WaitForSeconds(delay);
		}
	}
}
