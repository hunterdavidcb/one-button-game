﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
	public Transform rootBone;
	public GameObject maskInventory;
	int currentMasks = 5;
	int max = 5;

	public delegate void InventoryChangeHandler(int c);
	public event InventoryChangeHandler InventoryChanged;

	List<GameObject> inventoryMasks = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
		GetComponent<PickUp>().PickedUp += OnBoxPickedUp;
		GetComponent<DropMask>().Dropped += OnMaskDropped;

		for (int i = 0; i < max; i++)
		{
			GameObject go = Instantiate(maskInventory, rootBone);
			go.transform.localPosition = new Vector3(0.2f * i,
				go.transform.localPosition.y, go.transform.localPosition.z);
			inventoryMasks.Add(go);
		}
    }

	//we don't care about the input, but other classes do
	private void OnMaskDropped(GameObject mask)
	{
		currentMasks--;
		GameObject go = inventoryMasks[inventoryMasks.Count - 1];
		inventoryMasks.Remove(go);
		Destroy(go);
		if (InventoryChanged != null)
		{
			InventoryChanged(currentMasks);
		}

	}

	private void OnBoxPickedUp(int n)
	{
		currentMasks += n;
		if (currentMasks > max)
		{
			currentMasks = max;
		}

		int s = inventoryMasks.Count;
		Debug.Log(currentMasks);
		Debug.Log(s);
		int g = currentMasks - inventoryMasks.Count;
		Debug.Log(g);
		for (int i = 0; i < g; i++)
		{
			GameObject go = Instantiate(maskInventory, rootBone);
			go.transform.localPosition = new Vector3(0.2f * s,
				go.transform.localPosition.y, go.transform.localPosition.z);
			inventoryMasks.Add(go);
			s++;
		}

		if (InventoryChanged != null)
		{
			InventoryChanged(currentMasks);
		}
	}

	// Update is called once per frame
	void Update()
    {
        
    }
}
