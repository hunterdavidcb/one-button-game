﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	Vector2 mousePos;
	Vector2 playerPos;
	float safeDistance = 200f;
	Vector2 v;
	Animator anim;
	bool isRight = false;
    // Start is called before the first frame update
    void Start()
    {
		anim = GetComponent<Animator>();
		GetComponent<GetSick>().Died += OnDied;

		for (int i = 0; i < transform.childCount; i++)
		{
			if (transform.GetChild(i).GetComponent<Rigidbody2D>())
			{
				AlterRigidBodies(transform.GetChild(i), false);
			}
		}
    }

	void AlterRigidBodies(Transform t, bool b)
	{
		if (t.GetComponent<Rigidbody2D>())
		{
			t.GetComponent<Rigidbody2D>().simulated = b;
		}

		for (int i = 0; i < t.childCount; i++)
		{
			if (t.GetChild(i).GetComponent<Rigidbody2D>())
			{
				AlterRigidBodies(t.GetChild(i),b);
			}
		}
	}

	private void OnDied()
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			if (transform.GetChild(i).GetComponent<Rigidbody2D>())
			{
				AlterRigidBodies(transform.GetChild(i), true);
			}
		}

		this.enabled = false;
	}

	// Update is called once per frame
	void Update()
    {
		mousePos = Input.mousePosition;

		playerPos = Camera.main.WorldToScreenPoint(transform.position);

		if (Vector2.Distance(mousePos,playerPos) > safeDistance)
		{
			float s = Vector2.Distance(mousePos, playerPos) / 100;
			playerPos = Vector2.SmoothDamp(playerPos, mousePos, ref v, 5/s);
			anim.SetFloat("speed", s);
			transform.position = Camera.main.ScreenToWorldPoint(new Vector3(playerPos.x,playerPos.y,10f));
		}
		else
		{
			anim.SetFloat("speed", 0);
		}

		//use the dot product to check if the mouse is on the left or the right
		if (Vector3.Dot(Vector3.left,mousePos-playerPos) < 0f && !isRight)
		{
			//right facing
			isRight = true;
			transform.rotation = Quaternion.Euler(0, 180, 0);
		}
		else if (Vector3.Dot(Vector3.left, mousePos - playerPos) > 0f && isRight)
		{
			//left facing
			isRight = false;
			transform.rotation = Quaternion.Euler(0, 0, 0);
		}
    }
}
