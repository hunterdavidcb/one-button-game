﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reset : MonoBehaviour
{
	public GameObject panel;
    // Start is called before the first frame update
    void Start()
    {
		GetComponent<CameraFollow>().target.GetComponent<GetSick>().Died += OnDied;
    }

	private void OnDied()
	{
		panel.SetActive(true);
	}

	// Update is called once per frame
	void Update()
    {
        
    }

	public void ResetButton()
	{
		SceneManager.LoadScene(0);
	}
}
